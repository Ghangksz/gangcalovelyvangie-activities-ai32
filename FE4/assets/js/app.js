
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [7, 10, 9, 12, 13, 6],
      backgroundColor: 'rgb(0, 0, 255)',
    },
    {
      label: 'Returned Books',
      data: [4, 7, 10, 11, 8, 8],
      backgroundColor: 'rgba(255, 99, 71, .8)',
    }
  ],
  },
  options: {
  scales: {
  yAxes: [{
  ticks: {
  beginAtZero: true
}
}]
}
}
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
   datasets: [{
      data: [10, 20, 30, 20],
      backgroundColor: [
          'rgb(255, 99, 71, .8)',
          'rgb(128, 128, 128)',
          'rgb(255, 255, 0)',
          'rgba(27, 64, 230, .8)',
        ],
    }],

    labels: [
        'Fiction',
        'Biography',
        'Novel',
        'Horror',
    ]
  },
  options: {
  scales: {
  yAxes: [{
  ticks: {
  beginAtZero: true
}
}]
}
}
});


