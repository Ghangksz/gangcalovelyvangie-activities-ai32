<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = 
    [
        'name', 'author', 'copies', 'category_id'
    ];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function borrowedbook(){
        return $this->hasMany(BorrowedBook::class, 'book_id', 'id');
    }
    public function returnedbook(){
        return $this->hasMany(ReturnedBook::class, 'book_id', 'id');
    }
}
