<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
           'category_id' => 'bail|required|exists:categories,id',
           'name' => 'bail|required|max:200',
           'author' => 'bail|required|max:200',
           'copies' => 'bail|required|integer|gt:0'
            
        ];
    }
    /**
     * Get the error messages for defined validation rules.
     *
     * @return array
     */ 
    public function messages()
    {
        return[
            'name.required' => 'Name is required',
            'author.required' => 'Author is required',
            'copies.required' => 'Copies are required',
            'copies.integer' => 'Copies must be integer',
            'copies.gt' => "Copies must greater than 0",
        ];
    }
    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors()));
    }
}
