<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Book;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class BorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
            //
            $book = Book::find(request()->get('book_id'));

        if(!empty($book))
        {
            $copies = $book->copies;
        }else
        {
            $copies = request()->get('copies');
        }
        return[
            'book_id' => 'bail|required|exists:books,id',
            'copies' => ['required',"le: {$copies}", 'bail', 'gt:0'],
            'patron_id' => 'exists:patrons,id',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return[
            'patron_id.exists' => 'The patron doesnt exist',
            'copies.le' => 'The borrowed copies exceeded',
            'book_id.exists' => 'The book doesnt exist'
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()));
    }
}
