<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Models\BorrowedBook;

class ReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowedbook = BorrowedBook::where([['patron_id', request()->get('patron_id')], ['book_id', request()->get('book_id')]])->first();
        if(!empty($borrowedbook))
        {
            $copies = $borrowedbook->copies;
        }else
        {
            $copies = request()->get('copies');
        }
        return [
            //
            'patron_id' => 'exists:borrowed_books,patron_id',
            'copies' => ['gt:0', "le: {$copies}", 'required', 'bail'],
            'book_id' => 'bail|required|exists:borrowed_books,book_id'
        ];
    }
     /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return[
           'patron_id.exists' => 'The patron doesnt exists',
           'copies.le' => 'The borrowed copies exceeded',
           'book_id.exists' => 'The book doesnt exists'
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()));
    }
}
