<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class PatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'last_name' => 'bail|required|max:20',
            'first_name' => 'bail|required|max:20',
            'middle_name' => 'bail|required|max:20',
            'email' => 'bail|required|email|unique:patrons'

        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     * 
     * @return array
     */
    public function messages()
    {
        return[
           'last_name.required' => 'Last name is required',
           'first_name.required' => 'First name is required',
           'middle_name.required' => 'Middle name is required',
           'email.required' => 'Email address is required',
           'email.email' => 'Email address must be valid',
           'email.unique' => 'Email address must be unique'
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()));
    }
}
