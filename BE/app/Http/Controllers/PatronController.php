<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use Illuminate\Http\Requests\PatronRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return response()->json(Patron::paginate(5));
    }

    
    public function store(PatronRequest $request)
    {
       

        Patron::create($request->validated());
        return response()->json(['message' => 'Patron added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        return response()->json(Patron::findOrFail($id));
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, $id)
    {
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->validated());
        return response()->json(['message' => 'Patron updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Patron::where('id', $id)->delete();
        return response()->json(['message' => 'Patron deleted']);
    }
}
