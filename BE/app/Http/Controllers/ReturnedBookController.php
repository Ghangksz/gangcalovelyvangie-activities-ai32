<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReturnedBook;
use App\Http\Requests\ReturnedBookRequest;
use App\Models\BorrowedBook;
use App\Http\Controllers\Controller;


class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(ReturnedBook::with(['paton', 'book', 'book.category'])->get());
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        //
       $borrowedbook = BorrowedBook::where([['patron_id', $request->patron_id], ['book_id', $request->book_id]])->firstOrFail();
       if(empty($borrowedbook))
       {
           return response()->json(['message' => 'Book not found']);
       }else
       {
           if($borrowedbook->copies == $request->copies)
           {
               $borrowedbook->delete();
           }else
           {
               $borrowedbook->update(['copies' => $borrowedbook->copies - $request->copies]);
           }
       }
       $createreturnedbook = ReturnedBook::create($request->all());
       $returedbook = ReturnedBook::with(['book'])->find($createreturnedbook->id);
       $returedbook->book->update(['copies' => $returedbook->book->copies + $request->copies]);
       return response()->json(['message' => 'Book Returned']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }
}
