<?php

namespace App\Http\Controllers;


use App\Http\Controllers;
use App\Http\Requests\BorrowedBookRequest;
use App\Models\BorrowedBook;
use App\Http\Controllers\Controller;


class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function store(BorrowedBookRequest $request)
    {
        //
        $createborrowedbook = BorrowedBook::create($request->only(['book_id', 'patron_id', 'copies']));
        $borrowedbook = BorrowedBook::with(['book'])->find($createborrowedbook->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);
        return response()->json(['message' => 'Book borrowed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $borrowedbook = BorrowedBook::with(['book', 'patron', 'book.category'])->where('id', $id)->firstOrFail();
        return response()->json($borrowedbook);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
