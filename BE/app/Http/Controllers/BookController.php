<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json(Book::with(['category:id, category'])->paginate(5));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
       Book::create($request->validated());
       return response()->json(['message ' => 'Book added']);
    }

    /**
     * Display the specified book.
     * 
     * 
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        try{
            return response()->json(Book::with(['category:id, category'])->where('id', $id)->firstOrFail());
        }catch(ModelNotFoundException $ex){
            return response()->json(['message' => 'Book not found']);
        }
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        try{
            $book = Book::with(['category:id, category'])->where('id', $id)->firstOrFail();
            $book->update($request->validated());
            return response()->json(['message' => 'Book updated']);
        }catch (ModelNotFoundException $ex){
            return response()->json(['message' => 'Book not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
           $book = Book::where('id', $id)->firstOrFail();
           $book->delete();
           return response()->json(['message' => 'Book deleted']);
       }catch (ModelNotFoundException $ex){
           return response()->json(['message' => 'Book not found']);
       }
    }
}
