<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\ReturnedBookController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'books' => BookController::class,
    'patrons' => PatronController::class,
    ]);
Route::apiResource('borrowed_books', BorrowedBookController::class)->only(['index', 'store']);
Route::apiResource('categories', CategoryController::class)->only('index');
Route::apiResource('returned_books', ReturnedBookController::class)->only(['index','store']);
