import Vue from 'vue'
import router from './router'
import App from './App.vue'
import store from './store'
import { BootstrapVue} from 'bootstrap-vue';
import Toast from "vue-toastification";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "./assets/css/style.css";
import "vue-toastification/dist/index.css";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true
});
Vue.use(BootstrapVue);

Vue.config.productionTip = false

new Vue({
  router, store,
  render: function (h) {return h(App) }
}).$mount('#app')
