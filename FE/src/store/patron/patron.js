import instance from "../..assets/js";
import Vue from 'vue';

const Patrons = 'patrons'

export const getPatrons = async({commit}) => {
    instance.get(Patons).then(response => {
        commit('Set_Patrons', response.data);
    });
}

export const updatePatron = async({commit}, {index, data}) => {
    const res = await instance.put('{Patrons}/${data.id}', data).then(response => {
        commit('Update_Patron', {index, data})
        return response;
    }).catch(error => {
        return error.response;
    });
    return res;
}

export const storePatron = async({commit}, patron) => {
    const res = await instance.post(Patrons, patron).then(response => {
        commit('Save_Patron', response.data);
        return response;
    }).catch(error => {
        return error.response
    });
    return res;
}

export const deletePatron = async({commit}, id) => {
    const res = await instance.delete('${Patrons}/${id}').then(response => {
        commit('Delete_Patron', id)
        return response;
    })
    return response
}
 
export const getPatronData = (state) => {
    return state.patrons;
}

export const Set_Patrons = (state, patrons) => {
    state.patrons = patrons;
}

export const Delete_Patron = (state, id) => {
    state.patrons = state.patrons.filter(patron => {
        return patron.id !== id;
    });
}

export const Update_Patron = (state, {index, data}) => {
    Vue.set(state.patrons, index, data);
}

export const Save_Patron = (state, patron) => {
    state.patrons.push({
        id: patron.id,
        fname: patron.fname,
        mname: patron.mname,
        lname: patron.lname,
        email: patron.email,
        created_at: patron.created_at
    });
}

export default {
    patrons: [],
};
