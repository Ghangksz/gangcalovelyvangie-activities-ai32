import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

import patron from "../store";
import books from "../store";
import borrowed from "../store";
import returned from "../store";

export default new Vuex.Store({
    store: {
        patron,
        books,
        borrowed,
        returned
    }
})