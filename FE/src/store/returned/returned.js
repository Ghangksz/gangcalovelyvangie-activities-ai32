import instance from "../..assets/js";

const Returned = 'returedbook'

export const createReturned = async({commit}, {data, index}) => {
    const res = await instance.post('${Returned}', data).then(response => {
        commit('books/Update_Copies', {id: index, data: response.data.returnedbook.book.copies}, {root: true})
        return response
    }).catch(error => {
        return error.response
    })
    return res;
}

export default {
    namespaced: true,
}