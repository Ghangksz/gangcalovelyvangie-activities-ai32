import instance from "../..assets/js";

const Borrowed = 'borrowedbook'

export const createBorrowed = async({commit}, {data, index}) => {
    const res = await instance.post('${Borrowed}', data).then(response => {
        commit('books/Update_Copies', {id: index, data: response.data.borrowedbook.book.cpoies}, {root: true})
        return response
    }).catch(error => {
        return error.response
    })
    return res;
}
 
export default {
    namespaced: true,
}