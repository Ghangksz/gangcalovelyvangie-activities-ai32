import instance from "../..assets/js";
import Vue from 'vue';

const Books = 'books';
const Category = ' categories' 


export const getCategories = async({commit}) => {
    const res = await instance.get(Category).then(response => {
        commit('Set_Categories', response.data)
        return response
    }).catch(error => { 
        return error.response});
    return res;
}

export const getBooks = async({commit}) => {
    const res = await instance.get(Books).then(response => {
        commit('Set_Books', response.data)
        return error.response;
    })
    return res;
}

export const storeBook = async({commit}, book) => {
    const res = await instance.post('${Books}/${data.id}', data).then(response => {
        commit('Update_Book', {id: index, data: response.data.book});
        return response;
    }).catch(error => {
        return error.response
    });
    return res;
}

export const deleteBook = async({commit}, id) => {
    const res = await instance.delete('${Books}/${id}').then(response => {
        commit('Delete_Book', id);
        return response;
    });
    return res;
}

export const getBooksData = (state) => {
    return state.books;
}

export const Set_Categories = (state, categories) => {
    state.categories = categories
}

export const Set_Books = (state, books) => {
    state.books = books;
 }

 export const Update_Book = (state, {id, data}) => {
     Vue.set(state.books, id, data);
 }

 export const Delete_Book = (state, id) => {
     state.books = state.books.filter(book => {
         return book.id !== id;
     });
 }

 export const Save_Book = (state, book) => {
     state.books.push(book);
 }

 export const Update_Copies = (state, {id, data}) => {
     state.books[id].copies = data
 }

 export default {
     books: [],
     categories: []
 }


