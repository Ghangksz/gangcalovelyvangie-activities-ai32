import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/pages/Dashboard'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/patron',
        name: 'Patron Management',
        component: () => import(/* webpackChunkName: "patron" */ '../components/pages/Patron')
    },
    {
        path: '/book',
        name: 'Books Management',
        component: () => import(/* webpackChunkName: "book" */ '../components/pages/Book')

    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import(/* webpackChunkName: "settings" */ '../components/pages/Settings')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
export default router
